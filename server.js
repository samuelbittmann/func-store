const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const port = 80

const items = {
    1: {
        price: 1405,
        stock: 2
    },
    2: {
        price: 50310,
        stock: 1
    },
    3: {
        price: 4130,
        stock: 0
    },
    4: {
        price: 10100,
        stock: 10
    },
    5: {
        price: 11115,
        stock: 5
    },
    6: {
        price: 45505,
        stock: 3
    },
    7: {
        price: 99995,
        stock: 4
    },
    8: {
        price: 1500,
        stock: 1
    },
    9: {
        price: 1990,
        stock: 3
    }
}

const users = {
    123: {
        orders: [
            {
                orderId: 1,
                date: '2019-01-25 12:01:12',
                items: [1, 5, 7, 9],
                total: 14025
            }
        ],
        transactions: [
            {
                date: '2019-01-01 15:00:12',
                amount: 13025
            },
            {
                date: '2019-01-01 15:05:32',
                amount: 2000
            },
            {
                date: '2019-01-10 15:05:32',
                amount: 90000
            }
        ]
    }
}

const tokenCache = {
    "123abc456": {
        expires: '2019-12-25 16:20:00',
        connectedUserId: 123,
        rights: ['read_orders', 'write_orders']
    },
    "123abcX56": {
        expires: '2019-01-24 16:20:00',
        connectedUserId: 123,
        rights: ['read_orders']
    },
    "123abc4X6": {
        expires: '2019-12-24 16:20:00',
        connectedUserId: 123,
        rights: ['']
    },
    "124abc456": {
        expires: '2019-12-25 16:20:00',
        connectedUserId: 124,
        rights: ['read_orders']
    }
}

function getOrdersForUser(userId) {
    return users[userId].orders
}

function cond(condition, contTrue, contFalse) {
    if (condition && contTrue) {
        return contTrue()
    } else if (contFalse) {
        return contFalse()
    }
}

function validateToken(token) {
    return tokenCache[token] && Date.now() <= new Date(tokenCache[token].expires)
}

function extractTokenFromAuthString(authString) {
    return authString ? authString.split(' ')[1] : ''
}

function getTokenRights(token) {
    return tokenCache[token].rights
}

function all(predicate, list) {
    return list.reduce((accu, v) => accu && predicate(v), true)
}

function verifyAccess(token, rights) {
    if (!token) {
        return false
    }
    const tokenRights = getTokenRights(token)
    return validateToken(token) && all(v => tokenRights.includes(v), rights)
}

app.use(bodyParser.json())

app.get(
    '/users/:userId/orders',
    (req, res) => {
        const { userId } = req.params
        const { authorization } = req.headers
        const authToken = extractTokenFromAuthString(authorization)
        cond(
            verifyAccess(authToken, ['read_orders']),
            () => res.send(getOrdersForUser(userId)),
            () => res.send('You must be logged in')
        )
    }
)

function first(collection) {
    return collection[0]
}

function groupRec(groups, collection) {
    return cond(
        collection.length === 0,
        () => groups,
        () => groupRec(
            cond(
                groups.length > 0 && last(last(groups)) === last(collection), 
                () => groups.concat([groups.pop().concat([collection.pop()])]), 
                () => groups.concat([[collection.pop()]]) 
            ),
            collection
        )
    )
    
}

function group(collection) {
    return groupRec(
        [],
        collection.slice().sort()
    )
}

function onSuccess(result, continuation) {
    return result.success
        ? continuation(result.value)
        : result
}

function mapResult(result, mapSuccess, mapFailure) {
    return result.success
        ? mapSuccess(result.value)
        : mapFailure(result.error)
}

function validateOrderRequest(data) {
    const valid = data 
        && data.userId
        && data.items
        && data.items.length > 0
    return valid
        ? {success: true, value: '', error: ''}
        : {success: false, value: '', error: 'Some of the post parameters are missing'}
}

function validateUserBalance(userId) {
    const balance = users[userId].transactions.reduce((accu, t) => accu + t.amount, 0)
        - users[userId].orders.reduce((accu, o) => accu + o.total, 0)
    const valid = balance >= 0

    return valid
        ? {success: true, value: '', error: ''}
        : {success: false, value: '', error: `You have ${balance} cents in outstanding payments`}
}

function itemExists(itemId) {
    return typeof(items[itemId]) !== 'undefined'
}

function getItem(itemId) {
    return items[itemId]
}

function getNextId(collection, idSelector = e => e.id) {
    return collection.reduce((accu, v) => Math.max(accu, idSelector(v)), -1) + 1
}

function insertOrder(data) {
    users[data.userId].orders.push({
        orderId: getNextId(users[data.userId].orders, o => o.orderId),
        date: Date.now().toString(),
        items: data.items,
        total: data.items.map(getItem).reduce((accu, it) => accu + it.price, 0)
    })
    return {success: true, value: '', error: ''}
}

function last(collection) {
    return collection[collection.length - 1]
}

function checkStock(itemId, quantity) {
    return items[itemId].stock >= quantity
}

function removeFromStock(itemId, quantity) {
    return items[itemId].stock -= quantity
}

function createOrder(data) {
    const d = data
    return mapResult(
        onSuccess(
            validateOrderRequest(d),
            () => onSuccess(
                validateUserBalance(d.userId),
                () => onSuccess(
                    all(itemExists, data.items) ? {success: true, value: '', error: ''} : {success: false, value: '', error: 'Invalid item id(s) supplied.'},
                    () => onSuccess(
                        all(
                            g => checkStock(first(g), g.length),
                            group(data.items)
                        )
                            ? {success: true, value: '', error: ''}
                            : {success: false, value: '', error: 'Some items have insufficient stock'},
                        () => onSuccess(
                            all(
                                g => removeFromStock(first(g), g.length) >= 0,
                                group(data.items)
                            )
                                ? {success: true, value: '', error: ''}
                                : {success: false, value: '', error: 'Error when removing items from stock.'},
                            () => insertOrder(d)
                        )
                    ) 
                )
            )
        ),
        s => last(getOrdersForUser(d.userId)),
        e => e
    )
}

app.post(
    '/users/:userId/orders',
    (req, res) => {
        const { userId } = req.params
        const { authorization } = req.headers
        const authToken = extractTokenFromAuthString(authorization)
        cond(
            verifyAccess(authToken, ['write_orders']),
            () => res.send(createOrder(req.body)),
            () => res.send('You don\'t have access')
        )
    }
)

app.listen(port)